//
//  TMainVC.h
//  Treto
//
//  Created by Alexey on 26.02.14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMainVC : UIViewController <UIScrollViewDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@end
