//
//  TMainVC.m
//  Treto
//
//  Created by Alexey on 26.02.14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TMainVC.h"
#import "Constants.h"
#import "TDataSource.h"

typedef void (^completion)(UIImage *);

@interface TMainVC ()
@property (retain, nonatomic) NSArray *urlList;
@property (retain, nonatomic) NSMutableArray *imageViewList;
@property (retain, nonatomic) NSCache *imageCache;
@property (assign, nonatomic) CGFloat screenWidth;
@property (assign, nonatomic) CGFloat screenHeight;
@property (assign, nonatomic) NSNumber *currentPage;
@property (retain, nonatomic) UIImageView *currentImageView;
@property (retain, nonatomic) TDataSource *dataSource;
@property (copy, nonatomic) completion completionBlock;
@end

@implementation TMainVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self initURLList];
        [self initImageCache];
        self.imageViewList = [[NSMutableArray alloc] init];
        self.dataSource = [TDataSource sharedInstance];
        self.completionBlock = [self prepareCompletionBlock];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    [self initScrollView];
    [self currentPageCalc];
    [self checkCurrentImage];
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
////    [self addImage];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.urlList release];
    [self.imageCache release];
    [self.currentPage release];
    [self.imageViewList release];
    [self.currentImageView release];
    [self.dataSource release];
    [self.completionBlock release];
    
    [_scrollView release];
    [super dealloc];
}


#pragma mark - Internal
- (void)initURLList {
    self.urlList = @[IMAGEURL1, IMAGEURL2, IMAGEURL3, IMAGEURL4, IMAGEURL5];
}

- (void)initImageCache {
    self.imageCache = [[NSCache alloc] init];
    for (int i = 0; i < IMAGESCOUNT; i++) {
        [self.imageCache setObject:[NSNull null] forKey:@(i)];
    }
}

#pragma mark - ScrollView
- (void)initScrollView {
    [self resizeScrollView];
    for (int i = 0; i < IMAGESCOUNT; i++) {
        CGRect imageViewFrame = CGRectMake(self.screenWidth * i, 0.00f, self.screenWidth, self.screenHeight);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageViewFrame];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.scrollView addSubview:imageView];
        [self.imageViewList addObject:imageView];
        [imageView release];
    }
}

- (void)resizeScrollView {
//screen dimension
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    self.screenWidth = CGRectGetWidth(screenBounds);
    self.screenHeight = CGRectGetHeight(screenBounds);
//scrollView dimension
    self.scrollView.contentSize = CGSizeMake(self.screenWidth * IMAGESCOUNT, self.screenHeight);
}

- (void)currentPageCalc {
    self.currentPage = @(floor((self.scrollView.contentOffset.x - self.screenWidth / 2) / self.screenWidth) + 1);
}

#pragma mark - ScrollView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self currentPageCalc];
    [self checkCurrentImage];
    DLog(@"currentPage: %@", self.currentPage);
}

#pragma mark - Image
- (void)checkCurrentImage {
    self.currentImageView = self.imageViewList[[self.currentPage integerValue]];
    if (self.currentImageView.image == nil) {
        [self addImage];
    }
}

- (void)addImage {
    if ([self.imageCache objectForKey:self.currentPage] != [NSNull null]) {
        [self showImage:[self.imageCache objectForKey:self.currentPage]];
    } else {
        [self loadImage];
    }
}

- (void)showImage:(UIImage*)image {
    if (![image isKindOfClass:[UIImage class]]) {
        DLog(@"%@", @"It is not an image!!!");
        return;
    }
    
    self.currentImageView.image = image;
}

- (void)loadImage {
    NSString *url = self.urlList[[self.currentPage integerValue]];
    
    [self.dataSource loadImageWithURL:url completion:self.completionBlock];
}

- (void (^)(UIImage*))prepareCompletionBlock {
    void (^completionBlock)(UIImage *) = ^void(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showImage:image];
        });
    };
    return [completionBlock copy];
}


@end
