//
//  TDataSource.m
//  Treto
//
//  Created by Alexey on 26.02.14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TDataSource.h"
#import "TWebService.h"

static id sharedInstance;

@implementation TDataSource

#pragma mark - Init
- (instancetype) init {
    
    self = [super init];
    if(self){
        
    }
    return self;
}


+ (instancetype) sharedInstance {
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    });
    
    return sharedInstance;
}


+ (id) copyWithZone: (NSZone *) zone {
    return self;
}


+ (instancetype) allocWithZone: (NSZone *) zone {
    if (sharedInstance != nil) {
        return sharedInstance ;
    }
    else {
        return [super allocWithZone: zone];
    }
}


 - (instancetype) retain {
     return self;
 }
 
 
 - (oneway void) release {
 
 }
 
 - (instancetype) autorelease {
     return self;
 }
 
 - (NSUInteger) retainCount {
     return NSUIntegerMax;
 }
 
 - (void) dealloc {
     [super dealloc];
 }

#pragma mark - Image loading
- (void)loadImageWithURL:(NSString *)url completion:(void (^)(UIImage *))completionBlock {

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        TWebService *webService = [[TWebService alloc] init];
        [webService loadImageWithURLString:url completion:completionBlock];
        [webService release];
    });
}



@end
