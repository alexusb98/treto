//
//  TWebService.m
//  Treto
//
//  Created by Alexey on 26.02.14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TWebService.h"

typedef void (^completion)(UIImage *);

@interface TWebService ()
@property (retain, nonatomic) NSURLSession *session;
@property (copy, nonatomic) completion completion;
@end

@implementation TWebService


- (instancetype)init {
    self = [super init];
    if (self) {
        // Custom initialization
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    return self;
}

- (void)dealloc {
    [self.session release];
    [self.completion release];
    
    [super dealloc];
}


#pragma mark - Image loading
- (void)loadImageWithURLString:(NSString *)urlString completion:(void (^)(UIImage *))completionBlock {
    self.completion = completionBlock;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionDownloadTask *task = [self.session downloadTaskWithURL:url];
    [task resume];
}

#pragma mark - NSURLSessionDownloadDelegate metods
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    //move downloaded image to new place
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *pathes = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [pathes objectAtIndex:0];
    NSError *error;

    NSString *destinationPath = [NSString stringWithFormat:@"%@/%@", docsDir, location.lastPathComponent];
    if ([fileManager fileExistsAtPath:[location path]	]) {
        NSURL *destinationURL = [NSURL fileURLWithPath:destinationPath];
        [fileManager moveItemAtURL:location toURL:destinationURL error:&error];
        if (error) {
            [self errorHandler:error];
        }
    }
    //completion block
    UIImage *image = [UIImage imageWithContentsOfFile:destinationPath];
    if (self.completion) {
        self.completion(image);
    }
    //cleaning
    [self.session invalidateAndCancel];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    if (error) {
        [self errorHandler:error];
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
    if (error) {
        [self errorHandler:error];
    }
}

- (void)errorHandler:(NSError*)error {
    DLog(@"Error: %@", error.localizedDescription);
}

@end
