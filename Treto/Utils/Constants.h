//
//  Constants.h
//  Treto
//
//  Created by Alexey on 26.02.14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#pragma mark - Images
static NSInteger const IMAGESCOUNT = 5;

#pragma mark - Images URLs
static NSString* const IMAGEURL1 = @"http://treto.ru/img_lb/Settecento/.IT/per_sito/ambienti/01.jpg";
static NSString* const IMAGEURL2 = @"http://treto.ru/img_lb/Settecento/.IT/per_sito/ambienti/02.jpg";
static NSString* const IMAGEURL3 = @"http://treto.ru/img_lb/Settecento/.IT/per_sito/ambienti/03.jpg";
static NSString* const IMAGEURL4 = @"http://treto.ru/img_lb/Settecento/.IT/per_sito/ambienti/05.jpg";
static NSString* const IMAGEURL5 = @"http://treto.ru/img_lb/Settecento/.IT/per_sito/ambienti/08.jpg";

/*

 Требуется создать UIScrollView c горизонтальным постраничным пролистыванием. Всего будет 5 страниц, на каждой из них будет свое изображение.
 При перелистывании на очередную страницу, соответствующее изображение должно быть асинхронно загружено из Интернета и показано.
 В процессе загрузки изображения на его месте должен появляться прогресс-бар, отображающий ход загрузки.
 Должны поддерживаться горизонтальная и вертикальная ориентации экрана.
 
 
 URL изображений для каждой из 5 страниц:
 ссылка
 ссылка
 ссылка
 ссылка
 ссылка
 
 
 Задание предназначено для iPad и должно быть представлено в виде проекта для Xcode 5.x. Проект не должен использовать ARC. Необходимо уметь управлять памятью.
*/