//
//  TWebService.h
//  Treto
//
//  Created by Alexey on 26.02.14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TWebService : NSObject <NSURLSessionDownloadDelegate, NSURLSessionDelegate>

- (void)loadImageWithURLString:(NSString*)urlString completion:(void(^)(UIImage*))completionBlock;

@end
