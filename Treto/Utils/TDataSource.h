//
//  TDataSource.h
//  Treto
//
//  Created by Alexey on 26.02.14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDataSource : NSObject


+ (instancetype) sharedInstance;
- (void)loadImageWithURL:(NSString*)url completion:(void(^)(UIImage*))completionBlock;

@end
